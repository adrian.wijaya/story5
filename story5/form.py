from django import forms
from .models import Portfolio

class PortfolioForm(forms.ModelForm):
	class Meta:
		model = Portfolio
		fields = ['judul','tanggal_awal', 'tanggal_akhir', 'description','tempat','kategori']