from django.db import models

# Create your models here.
class Portfolio(models.Model):		
	CHOICES = (
		('Pribadi', 'Pribadi'),
		('Tugas Kuliah', 'Tugas Kuliah'),
		('Proyek Magang', 'Proyek Magang')
	)
	judul = models.CharField(max_length=60)
	tanggal_awal = models.DateTimeField(blank=True, null=True)
	tanggal_akhir = models.DateTimeField(blank=True, null=True)
	description = models.TextField()
	tempat = models.TextField()
	kategori = models.CharField(max_length=100, choices= CHOICES, default='Pribadi')